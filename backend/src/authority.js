const { schema } = require("./schema")
const { uniqueNamesGenerator, adjectives, colors, animals, countries, names } = require('unique-names-generator')
const { foods, verbage, emoji } = require('./data')
var randomColor = require('randomcolor');

const customConfigAnimal = {
    dictionaries: [emoji, colors, animals],
    separator: ' ',
    style: 'capital'
  };

module.exports = class Authority {
    constructor(state, env) {
        this.state = state
        this.env = env

        this.state.blockConcurrencyWhile(async () => {
            try {
                this.doc = await this.state.storage.get('doc') || schema.node("doc", null, [schema.node("paragraph", null, [
                    schema.text("This is a collaborative test document. Start editing to make it more interesting!")
                ])])
            } catch (err) {
                this.doc = "Yea this threw an error"
            }
            this.steps = await this.state.storage.get('steps') || []
            this.userIDs = await this.state.storage.get('uids') || []
            this.version = await this.state.storage.get('version') || this.steps.length
            this.sockets = await this.state.storage.get('sockets') || []
            this.users = await this.state.storage.get('users') || []
            this.userIdentity = await this.state.storage.get('identity') || {}
            this.timestamps = await this.state.storage.get('timestamps') || []
            this.cursors = await this.state.storage.get('cursors') || {}
        })

    }

    async fetch(request) {
        let url = new URL(request.url)
        let path = url.pathname.slice(1).split('/').slice(1)
    
        if (path.length == 1) {
            if (request.method == 'POST') {
            let name = await this.state.storage.get("name")

            if (name != null) {
                return new Response(JSON.stringify({'message': "Already Exists", 'internal-code': '832ec41c'}))
            }

            let storagePromise = this.state.storage.put("name", JSON.parse(await request.text())['name']);
            await storagePromise;
            return new Response(JSON.stringify({'message': "Successful write!", 'internal-code': 'b3fa2539'}))
            }
        }

        switch (url.pathname) {
            case "/websocket": {
                if (request.headers.get("Upgrade") != "websocket") {
                    return new Response("expected websocket", {status: 400});
                }
        
                let ip = request.headers.get("CF-Connecting-IP");
        
                let pair = new WebSocketPair();
                await this.handleSession(pair[1], ip);
        
                return new Response(null, { status: 101, webSocket: pair[0] });
            }
            default:
                return new Response("Not found", {status: 404});
        }
    }

    async handleSession(websocket, ip) {
        websocket.accept()

        this.sockets.push(websocket)
        let username = uniqueNamesGenerator(customConfigAnimal)
        while (this.users.includes(username)) username = uniqueNamesGenerator(customConfigAnimal)
        this.users.push(username)
        // websocket.send(JSON.stringify({name: this.users[this.users.length - 1]}))

        const currentID = crypto.randomUUID().replaceAll('-', '')
        const name = uniqueNamesGenerator(customConfigAnimal)
        const color = randomColor();

        this.userIdentity[currentID] = {
            name: name,
            color: color,
        }
    
        websocket.send(JSON.stringify({ 
            'identity': {
                uid: currentID,
                name: name,
                color: color,
            },
            'doc': {
                steps: this.steps, 
                clientIDs: this.userIDs
            },
            cursors: this.cursors,
            'version': this.steps.length, 
            'code': '67dfdd9b'
        }))

        this.cursors[currentID] = {
            name: name,
            color: color,
            pos: null
        }

        this.sockets = this.sockets.filter(socket => {
            try {
                socket.send(JSON.stringify({
                    type: 'new-user',
                    uid: currentID,
                    name: name,
                    color: color,
                }))
                return true
            } catch (err) {
                return false
            }
        })
    
        websocket.addEventListener("message", async msg => {
            const data = JSON.parse(msg.data)
            if (data['type']) {
                this.sockets = this.sockets.filter(socket => {
                    try {
                        socket.send(JSON.stringify(data))
                        return true
                    } catch (err) {
                        return false
                    }
                })

                if (data['type'] == 'move-user') {
                    this.cursors[data['uid']] = {
                        pos: data['pos'],
                        color: this.cursors[currentID]['color'],
                        name: this.cursors[currentID]['name']
                    }
                } else if (data['type'] == 'remove-user') {
                    delete this.cursors[data['uid']]
                }
            } else {

                if (data['version'] != this.steps.length) {
                    // websocket.send(JSON.stringify({'message': 'Your current verison is not consistent with the remote!', code: 'blank', 'remote-version': this.steps.length}))
                    return;
                }
                
                data.steps.forEach(step => {
                    this.steps.push(step)
                    this.userIDs.push(data.clientID)
                })

                this.timestamps.push(data.timestamp)
            
                this.sockets = this.sockets.filter(socket => {
                    try {
                        socket.send(JSON.stringify({timestamp: data.timestamp, steps: this.steps.slice(this.version), clientIDs: this.userIDs.slice(this.version)}))
                        return true
                    } catch (err) {
                        return false
                    }
                })

                this.version = this.steps.length
            
            }
        })

        websocket.addEventListener("close", async msg => {
            this.sockets = this.sockets.filter(socket => {
                try {
                    socket.send(JSON.stringify({
                        type: 'remove-user',
                        name: name,
                        color: color,
                        uid: currentID
                    }))

                    delete this.cursors[currentID]

                    return true
                } catch (err) {
                    return false
                }
            })
        })

    }
    
    
}