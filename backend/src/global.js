module.exports = class Global {
    constructor(state, env) {
        this.state = state
        this.env = env

        this.state.blockConcurrencyWhile(async () => {
            this.users = await this.state.storage.get('steps') || {}
        })
    
    }

    async fetch(request) {
        let url = new URL(request.url)

        if (request.headers.get("Upgrade") != "websocket") {
            return new Response("expected websocket", {status: 400});
        }

        let pair = new WebSocketPair();
        await this.handleSession(pair[1], ip);
        
        return new Response(null, {
            status: 101,
            webSocket: pair[0] 
        });
    }

    async handleSession(websocket) {
        websocket.accept()

        
    }
}