// In order for our ES6 shim to find the class, we must export it
// from the root of the CommonJS bundle
const Counter = require('./counter.js')
const Authority = require('./authority.js')
const Global = require('./global.js')
exports.Counter = Counter
exports.Authority = Authority
exports.Global = Global

// polyfills
if (typeof String.prototype.replaceAll == "undefined") {  
  String.prototype.replaceAll = function(match, replace) {  
    return this.replace(new RegExp(match, 'g'), () => replace);  
  }  
}

exports.handlers = {
  async fetch(request, env) {
    try {
      return await handleRequest(request, env)
    } catch (e) {
      return new Response(e.message)
    }
  },
}

async function handleRequest(request, env) {
  let url = new URL(request.url);
  let path = url.pathname.slice(1).split('/');

  if (!path[0]) {
    if (request.method == 'GET') {
      return new Response("I am Root")
    }
    return env.GLOBAL.get(env.GLOBAL.idFromName("global")).fetch(url, request);    
  }

  switch (path[0]) {
    case "room":
      return handleRoomRequest(path.slice(1), request, env);
    default:
      return new Response("Not found", {status: 404});
  }
}

async function handleRoomRequest(path, request, env) {
  // as of right now no requests are longer than (3 - 1) sections
  if (path.length > 3) {
    return new Response("Not found", {status: 404});
  }

  if (!path[1]) {
    switch (path[0]) {
      case "private": {
        if (request.method == "POST") {
          let id = env.AUTHORITY.newUniqueId()
          let obj = env.AUTHORITY.get(id)
          obj.fetch(request)
          return new Response(id.toString(), {headers: {"Access-Control-Allow-Origin": "*"}});
        }
      }
      case "public": {
        if (request.method == "POST") {
          let requestClone = request.clone()
          const name = JSON.parse(await requestClone.text())['name']
          let id = env.AUTHORITY.idFromName(name);
          let obj = env.AUTHORITY.get(id)
          let res = await obj.fetch(request)
          let dataRes = JSON.parse(await res.text())
          console.log(dataRes['internal-code'])
          if (dataRes['internal-code'] == '832ec41c') {
            return new Response(JSON.stringify({'message': "Page already exists!", 'internal-code': '579935de'}), {status: 400})
          } else if (dataRes['internal-code'] == 'b3fa2539') {
            await env.PUBLIC_ROOMS.put(name, id.toString())
            return new Response(JSON.stringify({'id': id.toString(), 'internal-code': '648ba10f'}), {headers: {"Access-Control-Allow-Origin": "*"}});
          } else {
            return new Response(JSON.stringify({'message': "Unknown Error!", 'internal-code': '15fdb2fc'}), {status: 400})
          }
        } else if (request.method == 'GET') {
          return new Response(JSON.stringify((await env.PUBLIC_ROOMS.list())['keys'].map(room => room['name'])))
          // return new Response(JSON.stringify(await Promise.all(roomIDS.map(async roomID => {
          //   return await env.PUBLIC_ROOMS.get(roomID);
          // }))))
        }
      }
      default:
        return new Response("Not found", {status: 404});
    }
  }
  let id = path[1]; //To be URL safe, we will default the endpoints to be /room/<status>/<id>
  console.log(id)
  let roomObject = env.AUTHORITY.get(env.AUTHORITY.idFromString(id));
  let newUrl = new URL(request.url);
  newUrl.pathname = "/" + path.slice(2).join("/");

  switch (path[0]) {
    case "private": {
      return new Response("Not found", {status: 404});
    }
    case "public": {
      return roomObject.fetch(newUrl, request);
    }
    default: 
      return new Response("Not found", {status: 404});
  }
}
