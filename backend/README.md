## Velocity Lab Backend

This folder contains all the code necessary to run our backend that supports our `https://labs.velocity.xyz` demo.  Instructions to run our demo locally are listed out below.

## Locally Running

This backend is built utilizing the power of Cloudflare Workers, and thus utilizes [wrangler](https://developers.cloudflare.com/workers/wrangler/) to deploy and run the code locally.  

To start out however, you will have to have the node package manager installed for both wrangler installation as well as installing the packages required to run our backend.  If you have `npm` installed on your computer, continue down to the instructions on installing our packages and running it locally.  If you do not have `npm` installed, I would reccomend installing it via [homebrew](https://formulae.brew.sh/formula/node) if you have a Mac, `sudo apt install npm` if you are on a linux distro that supports `sudo apt`, or the [node.js](https://nodejs.org/en/download/) downloader if you are on windows.

Once you have `npm`, move your terminal session into our `/backend` folder.  This is where all of our code is hosted for our backend and thus you have to be in this current folder within your terminal session to be able to run it.  After that, run `npm install` to install all of the node packages that are required for our backend to be able to run.

I would suggest following [Cloudflare's Wrangler "Getting Started"](https://developers.cloudflare.com/workers/wrangler/get-started/) to set up wrangler on your local machine.

After you have set up wrangler, you have to do one more step prior to being able to locally host this code.  Our team wrote some code to allow for public and private room generation, however it was never integrated into the final demo.  We figured this over-complicated the demo, as what we wanted to show was how powerful a Cloudflare backend was in supporting real-time software, and the ability to create new rooms would just clutter up the demo.  This code, however, is still in the backend.  This utilizes Cloudflares Key-Value database and thus you will have to create the KV database in your Cloudflare project.  The instructions for this are laid out in [Cloudflare's KV Documentation](https://developers.cloudflare.com/workers/wrangler/workers-kv/) however as that is more general documentation, here are the two lines you need to run in your terminal.

(This assumes you have your account id connected to a global variable in your terminal, if you do not add it to the `wrangler.toml` file)

```
wrangler kv:namespace create "PUBLIC_ROOMS"

wrangler kv:namespace create "PUBLIC_ROOMS" --preview
```

Copy and paste the `id` and `preview_id` values into the `wrangler.toml` file provided in this repository to replace our provided id's.

## Backend Developers

[Connor Klaassen](https://twitter.com/KlaassenConnor)

[Aryan Mathur](https://twitter.com/aryanm27)