## Velocity Lab Frontend

This folder contains all the code necessary to run and view our UI for the `https://labs.velocity.xyz` demo.  All instructions to be able to locally run and test this are laid out below.

## Locally Running

This frontend utilied the Node Package Manager to allow for the use of open-source external tools to speed up our own development as well as utilize software that improves the user experience that would take our small team of developers a while to code up on our own.  If you have `npm` installed on your computer, continue down to the instructions on installing our packages and running it locally.  If you do not have `npm` installed, I would reccomend installing it via [homebrew](https://formulae.brew.sh/formula/node) if you have a Mac, `sudo apt install npm` if you are on a linux distro that supports `sudo apt`, or the [node.js](https://nodejs.org/en/download/) downloader if you are on windows.

Once you have `npm`, move your terminal session into our `/frontend` folder.  This is where all of our code is hosted for our frontend and thus you have to be in this current folder within your terminal session to be able to run it.  After that, run `npm install` to install all of the node packages that are required for our frontend to be able to run.  If you are interested in what packages we are using, take a peek at our `package.json` file, where all of the packages that will be installed are located.  After you have installed the required node packages just run `npm run serve`, and our frontend javascript framework ([vue.js](https://vuejs.org)) will compile and locally host our code.  Go to the `localhost` port provided to you in your terminal session and you will see the exact same page that we have hosted live at `https://labs.velocity.xyz`.  As our backend is hardcoded into our frontend, you will still be streamed the same data that we are streaming to our live site.

## Frontend Developers

[Timmy Hein](https://twitter.com/talltimofficial)

[Erika Meredith](https://twitter.com/dommydoteth)

Zach Martin