import App from "./App.vue";
import { createApp } from "vue";
import "./styles.css";
import "./prosemirror.css";
import "./lab.css";

export const app = createApp(App);
app.mount("#app");
