# The Velocity Lab

Welcome to the Velocity Lab repository.  This is a repository for the Spring Cloudflare Competition in 2022.  For this competition, we were given the task of building a real-time collaboration application utilizing the power of Cloudflare's edge network.  

The timing of this competition couldn't have been more perfect for our team.  We are a small startup working on developing a collaborative workspace to help empower teams with remote, online communication.  For an online collaboration tool to be useful, it is required to have almost no latency.  Cloudflare's edge network enables our software to fulfil this requirement of collaboration software, allowing us to serve data to our end-users with very little latency.

## Our Team

[Timmy Hein](https://twitter.com/talltimofficial) ([GitLab Profile](https://gitlab.com/timothyhein))

[Connor Klaassen](https://twitter.com/KlaassenConnor) ([GitLab Profile](https://gitlab.com/connorklaassen))

[Erika Meredith](https://twitter.com/dommydoteth) ([GitLab Profile](https://gitlab.com/erikameredith))

[Aryan Mathur](https://twitter.com/aryanm27) ([GitLab Profile](https://gitlab.com/aryanmathur2003))

Zach Martin ([GitLab Profile](https://gitlab.com/zach_martin))

## Cloudflare Tools Utilized

- Pages:
  - Hosting of our frontend
  - Pipeline from GitLab for auto-deployemnts and preview builds
- DNS:
  - Allows our Cloudflare hosted site to be routed to through our custom domain
- Workers:
  - Hosting our backend on an edge network allowing low latency data transmission
- Durable Objects:
  - Temporarily storing data and propogating it across Cloudflare's edge network to allow every user to view the same information

## Our Lab

Our team decided to utilize this competiton to help flush out our collaborative editor that will end up becoming the backbone of our final product.  By taking the time to understand the power Cloudflare Workers and Durable Objects can add to a web socket, we were able to create a what you see is what you get (WYSIWYG) editor with low latency data propogation across the users connected to it.  Our lab is a single editor that multiple users can connect to, allowing them to view each others position in real-time, as well as view different statistics related to their session.

We were fortunate enough to have a member of our team in Europe during the development of this demo, allowing us to test out trans-atlantic latency.  With this we were able to discover that we could tranmit data from an individual in Europe to an individual in California faster than we were able to transmit data from an AWS data center to a user in the given AWS region.  With this we are able to build our online communcation platform with a latency lower than that of some widely used platforms such as Google Docs and Notion.  This is all achievable through the power of Cloudflare's Edge Network